import { Component, OnInit } from '@angular/core';
import {PokemonService} from '../../services/pokemon.service';

@Component({
  selector: 'app-pokemon',
  templateUrl: './pokemon.page.html',
  styleUrls: ['./pokemon.page.scss'],
})
export class PokemonPage implements OnInit {

  private pokemonList;

  constructor(private pokemonService: PokemonService) { }

  ngOnInit() {
    this.pokemonList = this.pokemonService.pokemonList;
  }

}
