import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreatePokemonPage } from './create-pokemon.page';

describe('CreatePokemonPage', () => {
  let component: CreatePokemonPage;
  let fixture: ComponentFixture<CreatePokemonPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreatePokemonPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreatePokemonPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
