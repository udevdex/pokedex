import {Attaque} from './Attaque';
import {Region} from './Region';
import {Talent} from './Talent';
import {Type} from './Type';

export class Pokemon {

    idNational: number;
    idRegional: number;
    name: string;
    image: string;
    description: string;
    firstType: Type;
    secondType: Type;
    weight: number;
    height: number;
    region: Region;
    attackList: Attaque[];
    talentList: Talent[];

    constructor() {}
}

