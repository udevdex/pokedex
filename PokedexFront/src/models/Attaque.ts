export class Attaque {

    idAttaque: number;
    name: string;
    image: string;
    type: string;
    puissance: number;
    précision: number;
    powerPoint: number;

    constructor() {}
}
