import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', loadChildren: '../pages/home/home.module#HomePageModule' },
  { path: 'pokemon', loadChildren: '../pages/pokemon/pokemon.module#PokemonPageModule' },
  { path: 'create-pokemon', loadChildren: '../pages/create-pokemon/create-pokemon.module#CreatePokemonPageModule' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
