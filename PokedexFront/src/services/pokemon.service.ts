import {Injectable} from '@angular/core';

@Injectable()
export class PokemonService {

    pokemonList = [
        {
            idNational: '001',
            name: 'Bulbizarre',
            image: '../../assets/image/001Bulbasaur.png',
            type1: 'plante',
            type2: 'poison',
            color1: 'rgb(101, 231, 101)',
            color2: 'rgb(144, 238, 144)'
        },
        {
            idNational: '002',
            name: 'Herbizarre',
            image: '',
            type1: 'plante',
            type2: 'poison',
            color1: 'rgb(101, 231, 101)',
            color2: 'rgb(144, 238, 144)'
        },
        {
            idNational: '003',
            name: 'Florizarre',
            image: '',
            type1: 'plante',
            type2: 'poison',
            color1: 'rgb(101, 231, 101)',
            color2: 'rgb(144, 238, 144)'
        },
    ];

    getAllPokemon() {}
    getPokemonByRegion() {}

}
