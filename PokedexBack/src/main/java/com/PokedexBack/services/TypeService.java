package com.PokedexBack.services;

import com.PokedexBack.helper.TypeMapper;
import com.PokedexBack.models.Type;
import com.PokedexBack.models.TypeJSON;
import com.PokedexBack.repository.TypeRepository;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

import java.util.List;

@Service
public class TypeService {

    @Resource
    private TypeRepository typeRepository;

    @Resource
    private TypeMapper mapper;

    public List<TypeJSON> getAllType() {
        List<Type> TypeList = typeRepository.findAll();
        return mapper.mapTo(TypeList);
    }
    public TypeJSON getTypeById(long id) {
    	Type c = typeRepository.getOne(id);
        return mapper.mapTo(c);
    }
    public TypeJSON addType(TypeJSON Type) {
    	Type c = typeRepository.save(mapper.mapTo(Type));
        return mapper.mapTo(c);
    }
    public void deleteType(TypeJSON Type) {
    	typeRepository.delete(mapper.mapTo(Type));
    }
}