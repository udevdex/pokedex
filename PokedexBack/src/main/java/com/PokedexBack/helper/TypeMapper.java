package com.PokedexBack.helper;

import com.PokedexBack.models.Type;
import com.PokedexBack.models.TypeJSON;

import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class TypeMapper {

    public Type mapTo(TypeJSON TypeJSON) {
        Assert.notNull(TypeJSON, "The userJSON must not be null");
        Type c = new Type();
        // must not set id !
        c.setNom(TypeJSON.getNom());
        c.setImage(TypeJSON.getImage());
        return c;
    }

    public TypeJSON mapTo(Type c) {
        Assert.notNull(c, "The user must not be null");
        TypeJSON cJSON = new TypeJSON();
        cJSON.setId(c.getId());
        cJSON.setNom(c.getNom());
        cJSON.setImage(c.getImage());
        return cJSON;
    }

    public List<TypeJSON> mapTo(List<Type> TypeList) {
        Assert.notNull(TypeList, "The userList must not be null");
        return TypeList.stream().map(Type -> this.mapTo(Type)).collect(Collectors.toList());
    }
}