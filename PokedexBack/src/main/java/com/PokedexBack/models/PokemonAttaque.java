package com.PokedexBack.models;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@IdClass(PokemonAttaque.class)
public class PokemonAttaque implements Serializable {

	@Id
	@ManyToOne
	@JoinColumn
	private Pokemon pokemon;
	
	@Id
	@ManyToOne
	@JoinColumn
	private Attaque attaque;
	
	private int niveauApprentissage;

	public Pokemon getPokemon() {
		return pokemon;
	}

	public void setPokemon(Pokemon pokemon) {
		this.pokemon = pokemon;
	}

	public Attaque getAttaque() {
		return attaque;
	}

	public void setAttaque(Attaque attaque) {
		this.attaque = attaque;
	}

	public int getNiveauApprentissage() {
		return niveauApprentissage;
	}

	public void setNiveauApprentissage(int niveauApprentissage) {
		this.niveauApprentissage = niveauApprentissage;
	}
	
	
}
