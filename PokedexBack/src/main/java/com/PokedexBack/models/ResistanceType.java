package com.PokedexBack.models;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@IdClass(ResistanceType.class)
public class ResistanceType implements Serializable {

	@Id
	@ManyToOne
	@JoinColumn
	private Type typeAttaque;
	
	@Id
	@ManyToOne
	@JoinColumn
	private Type typeDefense;
	
	private double resistance;

	public Type getTypeAttaque() {
		return typeAttaque;
	}

	public void setTypeAttaque(Type typeAttaque) {
		this.typeAttaque = typeAttaque;
	}

	public Type getTypeDefense() {
		return typeDefense;
	}

	public void setTypeDefense(Type typeDefense) {
		this.typeDefense = typeDefense;
	}

	public double getResistance() {
		return resistance;
	}

	public void setResistance(double resistance) {
		this.resistance = resistance;
	}
	
	
}
