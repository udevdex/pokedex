package com.PokedexBack.models;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Attaque {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	
	@OneToMany(mappedBy = "attaque", cascade = CascadeType.ALL)
	@JsonIgnore
	private List<PokemonAttaque> pokemons;
	
	@ManyToOne
	@JoinColumn
	@JsonIgnore
	private Type type;
	
	private long puissance;
	private long accuracy;
	private String categorie;
	private String image;
	private long pp;

}

