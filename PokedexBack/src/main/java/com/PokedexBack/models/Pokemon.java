package com.PokedexBack.models;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Pokemon {

	@Id
	private long id;
	private long idRegional;
	private String nom;
	private String photo;
	private String description;
	private double poids;
	private long taille;
	
	@ManyToOne
	@JoinColumn
	@JsonIgnore
	private Type type1;
	
	@ManyToOne
	@JoinColumn
	@JsonIgnore
	private Type type2;
	
	@ManyToOne
	@JoinColumn
	@JsonIgnore
	private Region region;
	
	@OneToMany(mappedBy = "pokemon", cascade = CascadeType.ALL)
	@JsonIgnore
	private List<PokemonAttaque> attaques;
	
	@ManyToMany
	@JoinColumn
	@JsonIgnore
	private List<Talent> talents;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getIdRegional() {
		return idRegional;
	}

	public void setIdRegional(long idRegional) {
		this.idRegional = idRegional;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public double getPoids() {
		return poids;
	}

	public void setPoids(double poids) {
		this.poids = poids;
	}

	public long getTaille() {
		return taille;
	}

	public void setTaille(long taille) {
		this.taille = taille;
	}

	public Type getType1() {
		return type1;
	}

	public void setType1(Type type1) {
		this.type1 = type1;
	}

	public Type getType2() {
		return type2;
	}

	public void setType2(Type type2) {
		this.type2 = type2;
	}

	public Region getRegion() {
		return region;
	}

	public void setRegion(Region region) {
		this.region = region;
	}

	public List<PokemonAttaque> getAttaques() {
		return attaques;
	}

	public void setAttaques(List<PokemonAttaque> attaques) {
		this.attaques = attaques;
	}

	public List<Talent> getTalents() {
		return talents;
	}

	public void setTalents(List<Talent> talents) {
		this.talents = talents;
	}
	
	
	
	
}
