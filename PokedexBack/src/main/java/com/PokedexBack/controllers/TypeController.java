package com.PokedexBack.controllers;

import org.springframework.http.MediaType;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import javax.annotation.Resource;
import javax.validation.Valid;

import com.PokedexBack.models.TypeJSON;
import com.PokedexBack.services.TypeService;

@CrossOrigin(origins = "http://localhost:8082")
@RestController
@RequestMapping("/type")
public class TypeController {
	
	@Resource
	private TypeService TypeService;
	
	@RequestMapping(method = RequestMethod.GET)
	public List<TypeJSON> getAll(){
		return TypeService.getAllType();
	}
	@RequestMapping(method = RequestMethod.GET, value = "/{id}")
	public TypeJSON getOneType(@PathVariable int id){
		return TypeService.getTypeById(id);
	}
	@RequestMapping(method = RequestMethod.POST, headers = {"Content-type=application/json"}, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.CREATED)
	public TypeJSON addType(@Valid @RequestBody TypeJSON Type) {
		return TypeService.addType(Type);
	}
	@RequestMapping(value = "/delete", method = RequestMethod.DELETE, headers = {"Content-type=application/json"}, consumes = MediaType.APPLICATION_JSON_VALUE)
	public String deleteType(@Valid @RequestBody TypeJSON Type) {
		TypeService.deleteType(Type);
		return "Type supprimé";
	}
}
