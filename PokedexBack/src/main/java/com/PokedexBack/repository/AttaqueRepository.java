package com.PokedexBack.repository;

import com.PokedexBack.models.Attaque;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AttaqueRepository extends JpaRepository<Attaque, Long>{

}
