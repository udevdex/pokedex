package com.PokedexBack.repository;

import com.PokedexBack.models.PokemonAttaque;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PokemonAttaqueRepository extends JpaRepository<PokemonAttaque, Long>{

}