package com.PokedexBack.repository;

import com.PokedexBack.models.ResistanceType;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ResistanceTypeRepository extends JpaRepository<ResistanceType, Long>{

}